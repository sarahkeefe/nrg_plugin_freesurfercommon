/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import java.util.ArrayList;

public class StdBuildLauncher extends PipelineLauncher{

	public static final String  NAME = "StdBuild.xml";
	public static final String  LOCATION = "build-tools";
	public static final	String STDBUILDTEMPLATE = "PipelineScreen_StdBuild.vm";

	public static final	String MPRAGE = "MPRAGE";
	public static final String MPRAGE_PARAM = "mprs";
	
	
	public static final	String T2 = "T2";
	
	public static final String T2_PARAM = "t2";
	public static final	String EPI = "BOLD";
	
	public static final String EPI_PARAM = "fstd";
	public static final	String DTI = "DTI";
	
	public static final String DTI_PARAM = "dti";
	public static final	String FLAIR = "FLAIR";


	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StdBuildLauncher.class);

	public boolean launch(RunData data, Context context) {
		boolean rtn = false;

		return rtn;
	}

	private ArrayList<String> getRunLabels(ArrayList<String> boldScans) {
		ArrayList<String> rtn = new ArrayList<String>();
		for (int i = 0; i < boldScans.size(); i++) {
			rtn.add("run"+(i+1));
		}
		return rtn;
	}

	private ArrayList<String> getRunLabels(ArrayList<String> boldScans,ArrayList<String> runLabels,ArrayList<String> functionalScans ) {
		ArrayList<String> rtn = new ArrayList<String>();
		for (int i = 0; i < functionalScans.size(); i++) {
			for (int j=0; j<boldScans.size(); j++) {
				if (boldScans.get(j).equals(functionalScans.get(i))) {
					rtn.add(runLabels.get(j));
					break;
				}
			}
		}
		return rtn;
	}


}
